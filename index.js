'use strict';

const baseDir      = `${process.cwd()}/`;
let   settingsFile = `${baseDir}connections.json`;

const argv = require('yargs')
  .usage('Uage: $0 [<options>] <table>')

  .demandCommand(1, 1)
  .command('table', 'The table for which the model should be created.')

  .alias('c', 'connections-file')
  .nargs('c', 1)
  .default('c', settingsFile)
  .describe('c', 'Connections JSON file defining data sources.')

  .alias('s', 'schema')
  .nargs('s', 1)
  .default('s', 'dbo')
  .describe('s', 'Name of schema.')

  .argv;

// Table for which the model shall be generated.
const tableName = argv._[0];

// Resolve settings file path.
const path = require('path');
const fs   = require('fs');
const schemaName = argv.schema;

if (path.isAbsolute(argv.c))
  settingsFile = path.resolve(argv.c);
else
  settingsFile = path.resolve(baseDir + argv.c);

if (!fs.existsSync(settingsFile)) {
  console.error(`Settings file (${settingsFile}) not found.`);
  return;
}

// Connect to the database.
const mssql    = require('mssql');
const connOpts = require(settingsFile);
let   conn;

new mssql.Connection(connOpts)
  .connect()
  .then(c => conn = c)
  // Generate the schema.
  .then(conn => {
    const Generator = require('ndm-schema-generator-mssql').MSSQLSchemaGenerator;
    const gen       = new Generator(conn);

    return gen
      .generateSchema(connOpts.database, argv.s);
  })
  // Build the model.
  .then(schema => {
    const table  = schema.tables
      .find(t => t.name === tableName);

    if (!table)
      throw new Error(`Table ${tableName} not found.`);

    // The model is built using a template.
    const format     = require('string-template');
    const modelTpl   = fs.readFileSync(`${__dirname}/modelTpl.txt`, 'utf8');
    const colTpl     = fs.readFileSync(`${__dirname}/columnTpl.txt`, 'utf8');
    const colTplDflt = fs.readFileSync(`${__dirname}/columnTplDflt.txt`, 'utf8');

    // Model parameters.
    const displayName = snakeToPascal(tableName);
    const mapTo       = snakeToCamel(tableName);
    const alias       = tableName.split('_')
      .map(part => part[0])
      .join('');
    const columns = table.columns
      .map(column => {
        const colName      = column.name;
        const isPrimary    = column.isPrimary  ? 'true' : 'false';
        const isNullable   = column.isNullable ? 'true' : 'false';
        const defaultValue = column.defaultValue;
        const maxLength    = column.maxLength || -1;
        const tpl          = defaultValue ? colTplDflt : colTpl;
        let   sqlType;

        switch (column.dataType) {
          case 'bit':
            sqlType = 'CF_SQL_BIT';
            break;
          case 'datetime':
          case 'smalldatetime':
            sqlType = 'CF_SQL_DATETIME';
            break;
          case 'int':
            sqlType = 'CF_SQL_INTEGER';
            break;
          case 'float':
            sqlType = 'CF_SQL_FLOAT';
            break;
          default:
            sqlType = 'CF_SQL_VARCHAR';
        }

        return format(
          tpl,
          {
            colName,
            isPrimary,
            isNullable,
            defaultValue,
            maxLength,
            sqlType
          });
      })
      .join('');

    let   model    = format(
      modelTpl,
      {
        tableName,
        displayName,
        mapTo,
        alias,
        columns,
        schemaName
      });

    console.log(model);

    conn.close();
  })
  .catch(err => {
    console.error('Exception.', err);
    conn.close();
  });

function snakeToPascal(str) {
  return snakeToCamel(
    str.replace(/^[a-z]/g, c => c.toUpperCase()));
}

function snakeToCamel(str) {
  return str.replace(/_[a-z]/g, c => c.substr(1).toUpperCase());
}
