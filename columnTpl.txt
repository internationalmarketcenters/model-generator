    <cfset this
      .addColumn(new common.api.Model.Column(
        name         = "{colName}",
        isPrimary    = {isPrimary},
        isNullable   = {isNullable},
        sqlType      = "{sqlType}",
        maxLength    = {maxLength}))>
